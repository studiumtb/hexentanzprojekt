# Hexentanz

This Project was developed by 
Tobias Braun (dragobattack@gmail.com)
Adrian Kempf (araspian@gmail.com)
Marco Kern (kern.marco2001@gmail.com)
Tadas Novikas (tadasnovikasde@gmail.com)
Alexander Nuding (alex.nuding@icloud.com)
as Part of the "Medial Arbeit" course at the "Hochschule Reutlingen".

You can find a playable executable in the Hexentanz.zip

![](/Images/GameScene.png.png)
