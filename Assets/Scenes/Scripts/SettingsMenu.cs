using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public Slider mainSlider;
    public Toggle toggle;
    public Toggle toggleFullscreen;

    public void Start()
    {
        toggleFullscreen = GameObject.Find("FullScreenToggle").GetComponent<Toggle>();
        toggleFullscreen.isOn = Manager.enableFullscreen;
        toggleFullscreen.onValueChanged.AddListener(delegate { ToggleFullscreenValueChangeCheck(); });

        mainSlider = GameObject.Find("Slider").GetComponent<Slider>();
        mainSlider.value = Manager.qualityLevel;
        mainSlider.onValueChanged.AddListener(delegate { SliderValueChangeCheck(); });

        toggle = GameObject.Find("PostProcessingToggle").GetComponent<Toggle>();
        toggle.isOn = Manager.enablePostProcessing;
        toggle.onValueChanged.AddListener(delegate { ToggleValueChangeCheck(); });
        
    }

    public void backToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void SliderValueChangeCheck()
    {
        Manager.qualityLevel = Convert.ToInt32(mainSlider.value);
        QualitySettings.SetQualityLevel(Manager.qualityLevel, Manager.enablePostProcessing);
    }

    public void ToggleValueChangeCheck()
    {
        Manager.enablePostProcessing = toggle.isOn;
        QualitySettings.SetQualityLevel(Manager.qualityLevel, Manager.enablePostProcessing);
    }

    public void ToggleFullscreenValueChangeCheck()
    {
        Manager.enableFullscreen = toggleFullscreen.isOn;
        Screen.SetResolution(Screen.width, Screen.height, Manager.enableFullscreen);
    }
}
