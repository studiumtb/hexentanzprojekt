using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayingPiece : MonoBehaviour
{
    public enum CurrentState { NotStarted, Active, ReachedTarget }

    public Field CurrentPosition;

    public Player Owner;

    private bool selectionEnabled = false;

    Color defaultColor;

    [SerializeField]
    private AudioClip _movePiece;
    [SerializeField]
    private AudioClip _castSpell;

    private AudioSource _soundPlayerMove;
    private AudioSource _soundPlayerMagic;
    public bool ShowColor
    {
        set
        {     
            if(value)
            {
                gameObject.GetComponentInChildren<Renderer>().material.color = Owner.Color;
            }
            else
            {
                gameObject.GetComponentInChildren<Renderer>().material.color = defaultColor;
            }
        }
    }

    public CurrentState State
    {
        get
        {
            if(CurrentPosition is StartField)
                return CurrentState.NotStarted;
            else if(CurrentPosition is RingField)
                return CurrentState.Active;
            else if(CurrentPosition is TargetField)
                return CurrentState.ReachedTarget;

            throw new Exception("Invalid Pice Position");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        defaultColor = gameObject.GetComponentInChildren<Renderer>().material.color;        
        ShowColor = false;

        _soundPlayerMove = gameObject.GetComponents<AudioSource>()[0];
        _soundPlayerMove.clip = _movePiece;
        _soundPlayerMagic = gameObject.GetComponents<AudioSource>()[1];
        _soundPlayerMagic.clip = _castSpell;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayMovingSound()
    {
        _soundPlayerMove.Play();
    }
    public void PlayMagic()
    {
        _soundPlayerMagic.Play();
    }

    public void SetSelectionEnabled(bool enabled)
    {
        selectionEnabled = enabled;
    }

    public bool GetSelectionEnabled()
    {
        return selectionEnabled;
    }
}