using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatenEffect : MonoBehaviour
{
    public Color effectColor;

    private Light _light;
    private ParticleSystem _smoke;

    [SerializeField]
    private float duration;

    private float _timer;
    private bool _isPlaying;

    // Start is called before the first frame update
    void Start()
    {
        _light = gameObject.GetComponent<Light>();
        _smoke = gameObject.GetComponent<ParticleSystem>();

        _smoke.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        if (_isPlaying)
        {
            _timer -= Time.deltaTime;
            if (_timer > 0)
            {
                _isPlaying = false;
                //_smoke.Stop();
            }
        }
    }

    public void PlayEffect()
    {
        _timer = duration;
        var main = _smoke.main;
        main.duration = duration;

        _light.color = effectColor;
        var col = _smoke.colorOverLifetime;
        col.enabled = true;

        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[]
        {
            new GradientColorKey(effectColor,0)
        }, new GradientAlphaKey[]
        {
            new GradientAlphaKey(0.0f,0),
            new GradientAlphaKey(1f, 0.5f),
            new GradientAlphaKey(0.0f, 0)
        });

        col.color = grad;

        _smoke.Play();
    }
}
