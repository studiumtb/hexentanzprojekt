using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Game : MonoBehaviour
{
    [Range(2, 6)]
    public int NumberOfPlayers;

    [Range(2, 6)]
    public int NumberOfFiguresPerPlayer;


    private GamePhase[] phases;

    public GamePhase startPhase;

    [SerializeField]
    private GamePhase currentPhase;

    PlayerManager players;
    public Player currentPlayer;

    public int WalkBackDistance
    {
        get
        {
            return FindObjectOfType<GeneratePath>().NumberOfFields / NumberOfPlayers + 1;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartNewGame();
    }

    public void StartNewGame()
    {
        SetupGame();

        if (startPhase != null)
        {
            startPhase.enabled = true;
            startPhase.EnterPhase();    
        }
    }

    private void Awake()
    {
        phases = FindObjectsOfType<GamePhase>();
        currentPhase = startPhase;
        QualitySettings.SetQualityLevel(Manager.qualityLevel, Manager.enablePostProcessing);
        Screen.SetResolution(Screen.width, Screen.height, Manager.enableFullscreen);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupGame()
    {
        players = FindObjectOfType<PlayerManager>();
        players.SetupPlayers();
        currentPlayer = players.players.First();
        GeneratePath pathGeneration = FindObjectOfType<GeneratePath>();
        pathGeneration.ReGenerateBoard();
        players.GeneratePlayingPieces();

        foreach(GamePhase phase in GetComponents<GamePhase>())
        {
            phase.Setup();
        }

        FindObjectOfType<UpdateDecoration>().SetupDecorations();
    }

    public void ChangeToPhase<NextPhase>() where NextPhase : GamePhase
    {
        var nextPhase = phases.FirstOrDefault(p => p is NextPhase);

        if (nextPhase != null)
        {            
            currentPhase.enabled = false;
            currentPhase = nextPhase;
            nextPhase.EnterPhase();
            nextPhase.enabled = true;
        }
    }
}
