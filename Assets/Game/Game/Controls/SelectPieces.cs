using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectPieces : MonoBehaviour
{
    public PlayingPiece SelectedPiece;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.allCameras.First().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selected = hit.collider.gameObject.GetComponentInParent<PlayingPiece>();
                if (selected != null && selected.GetSelectionEnabled())
                {
                    SelectedPiece = selected;
                }
            }
        }
    }
}
