using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private float keyboardRotationSpeed;
    [SerializeField]
    private float keyboardZoomSpeed;

    [SerializeField]
    private float mouseRotationSpeed;
    [SerializeField]
    private float mouseZoomSpeed;

    [SerializeField]
    private float LookAngle;
    [SerializeField]
    private float MaxLookAngle;
    [SerializeField]
    private float MinLookAngle;

    [SerializeField]
    private float Distance;
    [SerializeField]
    private float Angle;

    [SerializeField]
    private float MinDistance;
    [SerializeField]
    private float MaxDistance;

    private Transform CameraObject;
    private Transform CameraAngle;

    private Vector2 mouseDragStart;

    // Start is called before the first frame update
    void Start()
    {        
        CameraObject = gameObject.GetComponentInChildren<Camera>().gameObject.transform;
        CameraAngle = transform.Find("CameraAngle");
        CameraObject.transform.localPosition = new Vector3(0, 0, Distance);
    }

    [ExecuteInEditMode]
    void Update()
    {
        HandleCameraMouseDrag();
        HandleCameraPosition();
    }

    void HandleCameraPosition()
    {
        float rotationInput = Input.GetAxis("RotateCamera") * Time.deltaTime * keyboardRotationSpeed;
        if (rotationInput != 0)
            gameObject.transform.Rotate(new Vector3(0, rotationInput, 0));

        float zoomInput = Input.GetAxis("ZoomCamera") * Time.deltaTime * keyboardZoomSpeed * -1;
        if (zoomInput != 0)
        {
            Distance = Mathf.Clamp(CameraObject.transform.localPosition.z + zoomInput, MaxDistance * -1, MinDistance * -1);
            CameraObject.transform.localPosition = new Vector3(0, 0, Distance);
        }

        LookAngle = Mathf.Clamp(LookAngle, MinLookAngle, MaxLookAngle);
        CameraAngle.transform.localRotation = Quaternion.Euler(new Vector3(LookAngle, 0, 0));
    }

    void HandleCameraMouseDrag()
    {
        if(Input.GetMouseButtonDown(1))
        {
            mouseDragStart = Input.mousePosition;
        }
        if(Input.GetMouseButton(1))
        {
            float distance = mouseDragStart.x - Input.mousePosition.x;
            if (distance != 0)
            {
                float toRotate = distance / (Distance * 0.2f);

                gameObject.transform.Rotate(new Vector3(0, toRotate, 0));

                mouseDragStart = Input.mousePosition;
            }
        }
        if(Input.mouseScrollDelta.y != 0)
        {
            float zoomInput = Input.mouseScrollDelta.y * Time.deltaTime * mouseZoomSpeed;
            if (zoomInput != 0)
            {
                Distance = Mathf.Clamp(CameraObject.transform.localPosition.z + zoomInput, MaxDistance * -1, MinDistance * -1);
                CameraObject.transform.localPosition = new Vector3(0, 0, Distance);
            }
        }
    }
}
