using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : MonoBehaviour
{
    private PlayerManager playerManager;

    [SerializeField]
    private List<Field> nextFields;
    [SerializeField]
    private Field previousField;


    public Field PreviousField
    {
        get { return previousField; }
    }
    public Field[] NextFields
    {
        get { return nextFields.ToArray(); }
    }

    public PlayingPiece OnThis
    {
        get
        {
            return FindObjectsOfType<PlayingPiece>().Where(p => p.CurrentPosition == this).FirstOrDefault();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (nextFields.Count > 0)
        {
            foreach (Field f in nextFields)
            {
                if (f != null)
                    Gizmos.DrawLine(gameObject.transform.position, f.gameObject.transform.position);
            }
        }

        Gizmos.color = Color.blue;
        if (previousField != null)
            Gizmos.DrawLine(gameObject.transform.position, previousField.transform.position);
    }

    public void UpdatePreviousField(Field newPrev)
    {
        previousField = newPrev;
    }
    public void AddNextField(Field newNext)
    {
        nextFields.Add(newNext);
    }
}
