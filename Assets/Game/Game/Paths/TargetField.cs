using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetField : Field
{
    public Player Owner;
    public int Hirarchy;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(Owner != null)
        {
            if(!MakeMove.TargetHiglighted)
            {
                gameObject.GetComponent<Renderer>().material.color = Owner.Color;
            }
            else
            {
                Color fieldColor = gameObject.GetComponent<Renderer>().material.color;
                if (fieldColor != MakeMove.highlightColor && fieldColor != MakeMove.selectColor)
                {
                    gameObject.GetComponent<Renderer>().material.color = Owner.Color;
                }
            }
        }
    }
}
