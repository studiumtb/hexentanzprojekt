using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartField : Field
{
    public PlayingPiece StartsHere;
    public Player Owner;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Owner != null)
        {
            gameObject.GetComponent<Renderer>().material.color = Owner.Color;
        }
    }
}