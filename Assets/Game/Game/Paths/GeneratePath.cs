using Assets.Game;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GeneratePath : MonoBehaviour
{
    public float radius;
    public int startRadiusOffset;
    public float targetRadiusOffset;
    public int targetSpread;
    public float jitter;
    public int startRotationOffset;
    public float startRadius;

    Terrain terrain;

    private Game _game;
    private Game game
    {
        get
        {
            if(_game == null)
                _game = FindObjectOfType<Game>();
            return _game;
        }
    }

    public Transform field;
    public Transform start;
    public Transform target;

    private List<Field> _fields = new List<Field>();

    public int NumberOfFields
    {
        get
        {
            return game.NumberOfPlayers * (game.NumberOfFiguresPerPlayer + 5);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    private void Awake()
    {
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void ReGenerateBoard()
    {
        foreach(Field field in _fields)
        {
            Destroy(field.gameObject.transform.parent.gameObject);
        }
        _fields.Clear();
        GenerateBoard();
    }

    void GenerateBoard()
    {
        GenerateCircle();
        GenerateStarts();
        GenerateTargets();
    }
    
    /// <summary>
    /// Generate Main Circle
    /// </summary>
    void GenerateCircle()
    {
        //Get positions for all Fields of the Circle
        Vector3[] positions = getCirclePositions();
        int index = 0;
        //Create all Fields for the Circle
        foreach (Vector3 position in positions)
        {
            var newCircleField = GenerateFieldAtPosition(position,field);
            newCircleField.gameObject.name = "Circle " + index;
            index++;
        }

        //Connect all Fields
        for (int i = 1; i < NumberOfFields; i++)
        {
            _fields[i].UpdatePreviousField(_fields[i - 1]);
            _fields[i - 1].AddNextField(_fields[i]);
        }
        //Stitch Ends and start together
        _fields[0].UpdatePreviousField(_fields.Last());
        _fields.Last().AddNextField(_fields[0]);
    }

    /// <summary>
    /// Generate Starts for all Players
    /// </summary>
    void GenerateStarts()
    {
        PlayerManager players = FindObjectOfType<PlayerManager>();

        Vector3[] starts = getStarts();

        //Generate a Start for each Player
        for (int i = 0; i < game.NumberOfPlayers; i++)
        {
            Player currentPlayer = players.players[i];

            //Get Center of the Start
            Vector3 center = starts[i];

            //Get nearest Circle Field
            var entryField = GetClosestField(center).GetComponent<Field>();

            Vector3[] startFields = getFieldsForStart(center);

            List<StartField> generated = new List<StartField>();
            for (int j = 0; j < startFields.Length; j++)
            {
                var startField = GenerateFieldAtPosition(startFields[j], start);
                StartField newField = startField.GetComponentInChildren<StartField>();
                newField.Owner = currentPlayer;
                newField.AddNextField(entryField);

                generated.Add(newField);
                _fields.Add(newField);
            }

            currentPlayer.StartFields = generated.ToArray();
        }
    }

    void GenerateTargets()
    {
        PlayerManager players = FindObjectOfType<PlayerManager>();
        var targets = getTargets();

        for (int i = 0; i < game.NumberOfPlayers; i++)
        {
            Player currentPlayer = players.players[i];

            List<Field> generated = new List<Field>();

            var exitField = GetClosestField(targets[i,0]).GetComponent<Field>();
            for (int j = 0; j < game.NumberOfFiguresPerPlayer; j++)
            {
                Vector3 position = targets[i, j];

                var targetField = GenerateFieldAtPosition(position, target);
                TargetField newField = targetField.GetComponentInChildren<TargetField>();
                newField.Owner = currentPlayer;
                newField.Hirarchy = j;
                generated.Add(newField);
                _fields.Add(newField);
            }

            generated[0].UpdatePreviousField(exitField);
            exitField.AddNextField(generated[0]);

            for (int j = 0; j < generated.Count-1; j++)
            {
                generated[j].AddNextField(generated[j + 1]);
            }

            currentPlayer.TargetFields = generated.Select(f => f as TargetField).ToArray();
        }
    }

    Vector3[,] getTargets()
    {
        Vector3[,] targets = new Vector3[game.NumberOfPlayers,game.NumberOfFiguresPerPlayer];
        int targetOffset = 360 - 360 / (2 * game.NumberOfPlayers);
        float angleBetween = 360 / game.NumberOfPlayers;

        for (int i = 0; i < game.NumberOfPlayers; i++)
        {
            float angle = (angleBetween * i + startRotationOffset + targetOffset) % 360;

            for (int j = 0; j < game.NumberOfFiguresPerPlayer; j++)
            {                
                var targetStart = GenerationHelper.calculatePosition(gameObject.transform.position, angle, radius + targetRadiusOffset - j * targetSpread);

                targetStart.y = GenerationHelper.samplePosition(targetStart, field.gameObject, terrain);

                targets[i, j] = targetStart;
            }
        }

        return targets;
    }

    /// <summary>
    /// Get Ring Field closest to a Position
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    GameObject GetClosestField(Vector3 position)
    {
        float dist = float.MaxValue;
        GameObject closest = null;
        foreach(var field in _fields)
        {
            if (field.GetComponent<Field>() is RingField)
            {
                float distance = Vector3.Distance(position, field.gameObject.transform.position);
                if (distance < dist)
                {
                    closest = field.gameObject;
                    dist = distance;
                }
            }
        }
        return closest;
    }

    /// <summary>
    /// Generate a Field at a Position from the given Prefab
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="prefab"></param>
    /// <returns></returns>
    GameObject GenerateFieldAtPosition(Vector3 pos, Transform prefab)
    {
        var newField = Instantiate(prefab);
        newField.position = pos;
        newField.transform.parent = gameObject.transform;
        _fields.Add(newField.gameObject.GetComponentInChildren<Field>());
        return newField.gameObject;
    }

    /// <summary>
    /// Calculate Positions for all Ring Fields
    /// </summary>
    /// <returns></returns>
    private Vector3[] getCirclePositions()
    {
        if (terrain == null)
            terrain = GameObject.Find("MapTerrain").GetComponent<Terrain>();

        //Apply Jitter only if Game is Running
        float jitterToApply = 0;
        if (Application.isPlaying)
            jitterToApply = jitter;

        List<Vector3> positions = new List<Vector3>();
        //Angle between two fields
        float angleBetween = 360.0f / NumberOfFields;
        //Calculate Positions for all fields
        for (int i = 0; i < NumberOfFields; i++)
        {
            //Calculate new Position
            var newPosition = GenerationHelper.calculatePosition(gameObject.transform.position, i * angleBetween, radius,
                Random.Range(-jitterToApply, jitterToApply));

            //Adjust Height to Terrain
            newPosition.y = GenerationHelper.samplePosition(newPosition,field.gameObject,terrain);
            //Add new Position to List
            positions.Add(newPosition);
        }
        //return all Positions
        return positions.ToArray();
    }
    /// <summary>
    /// Calculate Positions for the Centers of all Starts
    /// </summary>
    /// <returns></returns>
    public Vector3[] getStarts()
    {
        List<Vector3> positions = new List<Vector3>();
        int numberOfPlayers = FindObjectOfType<Game>().NumberOfPlayers;
        float angleBetween = 360 / numberOfPlayers;
        for(int i = 0; i < numberOfPlayers; i++)
        {
            float angle = (angleBetween * i + startRotationOffset) % 360;
            var startCenter = GenerationHelper.calculatePosition(gameObject.transform.position, angle, radius + startRadiusOffset);

            startCenter.y = GenerationHelper.samplePosition(startCenter, field.gameObject, terrain);

            positions.Add(startCenter);
        }
        return positions.ToArray();
    }

    /// <summary>
    /// Calculate Positions for the Fields of a Start with the given Center
    /// </summary>
    /// <param name="center"></param>
    /// <returns></returns>
    private Vector3[] getFieldsForStart(Vector3 center)
    {
        float angleToCenter = Vector3.Angle(Vector3.forward, center);

        float numberOfFigures = game.NumberOfFiguresPerPlayer;
        List<Vector3> positions = new List<Vector3>();
        for (int j = 0; j < numberOfFigures; j++)
        {
            float angle = ((360 / numberOfFigures) * j + angleToCenter) % 360;

            Vector3 startFieldPosition = GenerationHelper.calculatePosition(center,
                angle, startRadius, 0);

            startFieldPosition.y = GenerationHelper.samplePosition(startFieldPosition,
                field.gameObject.GetComponentInChildren<Renderer>().bounds.size.x, terrain);

            positions.Add(startFieldPosition);
        }
        return positions.ToArray();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);

        if (!Application.isPlaying)
        {
            Vector3[] positions = getCirclePositions();
            for (int i = 0; i < positions.Length; i++)
            {
                Gizmos.DrawSphere(positions[i], 1);
            }

            Gizmos.color = Color.blue;
            Vector3[] startPositions = getStarts();
            for (int i = 0; i < startPositions.Length; i++)
            {
                Gizmos.DrawSphere(startPositions[i], 0.6f);

                var fields = getFieldsForStart(startPositions[i]);
                for(int j = 0; j < fields.Length; j++)
                {
                    Gizmos.DrawSphere(fields[j], 0.3f);
                }
            }

            Gizmos.color = Color.red;
            Vector3[,] targetPositions = getTargets();
            for(int i = 0; i < targetPositions.GetLength(0); i++)
            {
                for(int j = 0; j < targetPositions.GetLength(1); j++)
                {
                    Gizmos.DrawSphere(targetPositions[i, j], 0.3f);
                }
            }
        }
    }
}
