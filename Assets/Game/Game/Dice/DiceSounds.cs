using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSounds : MonoBehaviour
{
    [SerializeField]
    AudioClip impactSound;
    [SerializeField]
    AudioClip landedSound;

    AudioSource _audioSource;

    Rigidbody _body;

    bool _moving = false;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
        _body = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PlayLanded()
    {
        _audioSource.clip = landedSound;
        _audioSource.Play();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.relativeVelocity.magnitude > 1)
        {
            _audioSource.clip = impactSound;
            _audioSource.Play();
        }
    }
}
