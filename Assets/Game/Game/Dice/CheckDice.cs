using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDice : MonoBehaviour
{
    public bool diceThrown;
    private RollDice rollDice;
    private DiceSounds diceSounds;

    // Start is called before the first frame update
    void Start()
    {
        diceThrown = false;
        rollDice = FindObjectOfType<RollDice>();
        diceSounds = FindObjectOfType<DiceSounds>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerStay(Collider col)
    {
        if (diceThrown && col.gameObject.name != "Floor" && col.gameObject.name != "DiceCheckZone")
        {
            if (rollDice.diceVelocity.x == 0f && rollDice.diceVelocity.y == 0f && rollDice.diceVelocity.z == 0f &&
                Mathf.Round(col.gameObject.transform.rotation.eulerAngles.x) % 90 < 3 && Mathf.Round(col.gameObject.transform.rotation.eulerAngles.z) % 90 < 3)
            {
                switch (col.gameObject.name)
                {
                    case "Side1": rollDice.diceValue = 6; break;

                    case "Side2": rollDice.diceValue = 5; break;

                    case "Side3": rollDice.diceValue = 4; break;

                    case "Side4": rollDice.diceValue = 3; break;

                    case "Side5": rollDice.diceValue = 2; break;

                    case "Side6": rollDice.diceValue = 1; break;
                }

                diceSounds.PlayLanded();
                diceThrown = false;
            }            
        }
    }

}
