using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnimateMove : GamePhase
{
    /// <summary>
    /// Piece to Move
    /// </summary>
    public PlayingPiece selected;
    /// <summary>
    /// Field to Move Piece to
    /// </summary>
    public Field target;
    /// <summary>
    /// Number of remaining Steps
    /// </summary>
    public int stepCount;
    /// <summary>
    /// Next Field to move to in the Animation
    /// </summary>
    private Field _movingTo = null;
    /// <summary>
    /// Movement Speed of the Pieces
    /// </summary>
    public float Speed = 1;

    public override void EnterPhase()
    {
    }
    public override void LeavePhase()
    {
        //Clear all Parameters
        selected = null;
        target = null;
        stepCount = 0;
        _movingTo = null;
    }
    public override void Setup()
    {
    }
    private void Update()
    {
        //Not currently between two Fields
        if(_movingTo == null)
        {
            //Have we reached our Target?
            if(selected.CurrentPosition == target)
            {
                //Ist the Target a Target Field?
                if (target is TargetField)
                {
                    GetPhase<PieceEnteredTarget>().target = selected;
                    GetPhase<PieceEnteredTarget>().endField = (TargetField)target;
                    //The Piece has Entered a Target Field
                    ChangeToPhase<PieceEnteredTarget>();
                }
                else
                {
                    //Players Turn is over
                    ChangeToPhase<EndTurn>();
                }
            }
            else
            {
                //Is our Target a Ring field
                if(target is RingField)
                {
                    //Just move to next Ring field
                    _movingTo = selected.CurrentPosition.NextFields.First(f => f is RingField);
                }
                else if(target is TargetField tf)//The Target a Target Field
                {
                    //Are there any Target Fields we can directly move to?
                    var potentialExit = selected.CurrentPosition.NextFields.FirstOrDefault(f => f is TargetField) as TargetField;
                    if (potentialExit != null)
                    {
                        //Is the Target Field from the same Exit we want to take
                        if (potentialExit.Owner == tf.Owner)
                        {
                            //Move to Target Field
                            _movingTo = potentialExit;
                        }
                        else
                        {
                            //Stay on the Ring
                            _movingTo = selected.CurrentPosition.NextFields.First(f => f is RingField);
                        }
                    }
                    else
                        _movingTo = selected.CurrentPosition.NextFields.First(f => f is RingField);
                }

                //The next Field is our target, but there is already another playing piece there
                if(_movingTo == target && _movingTo.OnThis != null)
                {
                    //Change to Beat ohter Pice Phase
                    GetPhase<BeatOtherPiece>().attacker = selected;
                    GetPhase<BeatOtherPiece>().victim = _movingTo.OnThis;
                    ChangeToPhase<BeatOtherPiece>();
                }
                else
                {
                    selected.PlayMovingSound();
                }
            }
        }
        else//Currently animating a Transition
        {
            //Get distance and direction to target position
            Vector3 remainingDistance = _movingTo.gameObject.transform.position - selected.gameObject.transform.position;
            //Is the piece close Enough
            if (remainingDistance.magnitude > 0.001f)
            {
                //Calculate normal Step distance
                Vector3 stepMove = remainingDistance.normalized * Speed * Time.deltaTime;
                //Are we closer than one Step? If so, move only to exact target location
                Vector3 toMove = (remainingDistance.magnitude < stepMove.magnitude) ? remainingDistance : stepMove;

                selected.gameObject.transform.position += toMove;
            }
            else
            {
                selected.CurrentPosition = _movingTo;
                _movingTo = null;
                stepCount--;
            }
        }
    }

    private void OnGUI()
    {
        GUIStyle diceFont = new GUIStyle() { fontSize = 120 };
        diceFont.normal.textColor = CurrentPlayer.Color;
        GUI.Label(new Rect(Screen.width - 60, 10, 150, 150), stepCount.ToString(), diceFont);
    }
}
