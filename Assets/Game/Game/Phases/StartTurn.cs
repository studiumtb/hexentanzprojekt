using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTurn : GamePhase
{
    double start;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        start = Time.time;
    }
    public override void LeavePhase()
    {
    }
    public override void Setup()
    {
    }
    private void Update()
    {
        if(Time.time - start >= 4 || Input.GetButtonDown("Interact"))
            ChangeToPhase<RollDice>();
    }
    private void OnGUI()
    {        
        GUIStyle labelFont = new GUIStyle() { fontSize = 40 };
        labelFont.normal.textColor = CurrentPlayer.Color;
        GUI.Label(new Rect(10, 10, 1000, 100), "It's " + CurrentPlayer.Name + "'s turn", labelFont);
    }
}
