﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerWon : GamePhase
{
    public Player Winner;

    public override void EnterPhase()
    {

    }
    public override void Setup()
    {
    }
    public override void LeavePhase()
    {
    }
    private void Update()
    {
        if (Input.GetButtonDown("Interact"))
            ChangeToPhase<PrepareGame>();
    }
    private void OnGUI()
    {
        GUIStyle labelFont = new GUIStyle() { fontSize = 40 };
        labelFont.normal.textColor = CurrentPlayer.Color;
        GUI.Label(new Rect(Screen.width / 2 - 250, Screen.height / 2, 1000, 100), "Player " + Winner.Name + " won the Game!", labelFont);
        GUIStyle Font2 = new GUIStyle() { fontSize = 20 };
        Font2.normal.textColor = Winner.Color;
        GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 50, 1000, 100), "Press Space to start a new round.", Font2);
    }
}
