using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurn : GamePhase
{
    private PlayerManager _playerManager;
    private Game _game;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        int currentIndex = _playerManager.players.IndexOf(CurrentPlayer);
        int nextIndex = (currentIndex + 1) % _game.NumberOfPlayers;
        _game.currentPlayer = _playerManager.players[nextIndex];
        ChangeToPhase<StartTurn>(); 
    }
    public override void LeavePhase()
    {
    }
    public override void Setup()
    {
        _playerManager = FindObjectOfType<PlayerManager>();
        _game = FindObjectOfType<Game>();
    }
    private void Update()
    {
    }
}
