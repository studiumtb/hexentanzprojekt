﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class LeaveStart : GamePhase
{
    public int DiceValue;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        var toMove = CurrentPlayer.PlayingPieces.Where(p => p.State == PlayingPiece.CurrentState.NotStarted).First();
        Field iterator = toMove.CurrentPosition;
        for(int i = 0; i < DiceValue; i++)
        {
            iterator = iterator.NextFields.First(f => f is RingField);
        }
        GetPhase<AnimateMove>().target = iterator;
        GetPhase<AnimateMove>().selected = toMove;
        GetPhase<AnimateMove>().stepCount = DiceValue;
        ChangeToPhase<AnimateMove>();
    }
    public override void LeavePhase()
    {
    }
    public override void Setup()
    {
    }
    private void Update()
    {
    }
}