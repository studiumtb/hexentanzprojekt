﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class CheckForWinner : GamePhase
{
    List<Player> _allPlayers;
    Player Winner;
    bool allInTarget;

    public override void EnterPhase()
    {
        foreach(var check in _allPlayers)
        {
            allInTarget = true;
            foreach(var playingPiece in check.PlayingPieces)
            {
                if(!(playingPiece.CurrentPosition is TargetField))
                {
                    allInTarget = false;
                    break;
                }
            }
            if(allInTarget)
            {
                Winner = check;
                break;
            }
        }
    }
    public override void LeavePhase()
    {
    }
    public override void Setup()
    {
        _allPlayers = FindObjectOfType<PlayerManager>().players;
    }
    private void Update()
    {
        if (allInTarget)
        {
            GetPhase<PlayerWon>().Winner = Winner;
            ChangeToPhase<PlayerWon>();
        }
        else
        {
            ChangeToPhase<EndTurn>();
        }
    }
}
