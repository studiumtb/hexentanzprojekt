using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePhase : MonoBehaviour
{
    Game _parent;

    protected Game Parent
    {
        get
        {
            return _parent;
        }
    }
    protected Player CurrentPlayer
    {
        get
        {
            return _parent.currentPlayer;
        }
    }

    private void Awake()
    {
        _parent = FindObjectOfType<Game>();
        this.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public virtual void Setup()
    {

    }

    public virtual void EnterPhase()
    {
    }
    public virtual void LeavePhase()
    {
    }

    protected void ChangeToPhase<NextPhase>() where NextPhase : GamePhase
    {
        LeavePhase();
        _parent.ChangeToPhase<NextPhase>();
    }
    protected Phase GetPhase<Phase>() where Phase : GamePhase
    {
        return gameObject.GetComponent<Phase>();
    }

    protected bool AnimateMove(GameObject piece, Vector3 targetPosition, float threshold, float speed)
    {
        Vector3 remainingDistance = piece.gameObject.transform.position - targetPosition;
        if (remainingDistance.magnitude > threshold)
        {
            Vector3 stepMove = remainingDistance.normalized * speed * Time.deltaTime;
            Vector3 toMove = (remainingDistance.magnitude < stepMove.magnitude) ? remainingDistance : stepMove;

            piece.gameObject.transform.position -= toMove;
            return false;
        }
        else
        {
            return true;
        }
    }
}
