using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RollDice : GamePhase
{
    public Vector3 diceVelocity;

    public int diceValue;

    public float ThrowVelocity = 1000;
    public float RotationVelocity = 600;

    CheckDice checkDice;

    GameObject dice;

    bool diceRolled = false;

    double resultTime = 0;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        dice = GameObject.Find("Dice");
        diceValue = 0;
        resultTime = 0; 
    }
    public override void LeavePhase()
    {        

    }

    public override void Setup()
    {
        checkDice = FindObjectOfType<CheckDice>();
    }
    private void Update()
    {
        diceVelocity = dice.GetComponent<Rigidbody>().velocity;

        if (diceValue == 0)
        {
            if (checkDice.diceThrown)
            {
                if (diceVelocity.x == 0f && diceVelocity.y == 0f && diceVelocity.z == 0f)
                {
                    dice.GetComponent<Rigidbody>().AddForce(Random.Range(0, 5), Random.Range(0, 5), Random.Range(0, 5));
                }
            }
            else if (Input.GetButtonDown("Interact") && diceVelocity == Vector3.zero)
            {
                diceValue = 0;
                float dirX = Random.Range(0, RotationVelocity);
                float dirY = Random.Range(0, RotationVelocity);
                float dirZ = Random.Range(0, RotationVelocity);
                float tiltX = Random.Range(-8, 8);
                float tiltY = Random.Range(-8, 8);
                float tiltZ = Random.Range(-8, 8);
                dice.transform.localPosition += new Vector3(0, 0.1f, 0f);
                dice.transform.rotation = Quaternion.Euler(tiltX, tiltY, tiltZ);
                dice.GetComponent<Rigidbody>().AddForce(transform.up * ThrowVelocity);
                dice.GetComponent<Rigidbody>().AddTorque(dirX, dirY, dirZ);
                checkDice.diceThrown = true;
            }
        }
        else if(resultTime == 0)
        {
            checkDice.diceThrown = false;
            Debug.Log("Rolled " + diceValue);
            diceRolled = true;
            resultTime = Time.time;
        }
        else if(Time.time - resultTime > 3 || Input.GetButtonDown("Interact"))
        {
            if (CurrentPlayer.PlayingPieces.Any(p => p.State == PlayingPiece.CurrentState.NotStarted))
            {
                GetPhase<LeaveStart>().DiceValue = diceValue;
                ChangeToPhase<LeaveStart>();
            }
            else
            {
                GetPhase<MakeMove>().DiceValue = diceValue;
                ChangeToPhase<MakeMove>();
            }
        }
    }
    private void OnGUI()
    {
        if (diceValue == 0 && !checkDice.diceThrown)
        {
            GUIStyle labelFont = new GUIStyle() { fontSize = 40 };
            labelFont.normal.textColor = CurrentPlayer.Color;
            GUI.Label(new Rect(10, 10, 1000, 100), "Press [Space] to roll dice", labelFont);
        }
        else if(diceValue != 0)
        {
            GUIStyle diceFont = new GUIStyle() { fontSize = 120 };
            diceFont.normal.textColor = CurrentPlayer.Color;
            GUI.Label(new Rect(Screen.width / 2 - 20, Screen.height / 2 - 60, 40, 40), diceValue.ToString(), diceFont);
        }
    }
}
