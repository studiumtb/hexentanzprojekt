using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Get Player Input
/// </summary>
public class MakeMove : GamePhase
{
    public int DiceValue;
    public PlayingPiece SelectedPiece;
    public Field SelectedField;
    private SelectPieces _pieceSelector;
    public static bool TargetHiglighted;
    private Color defaultColor;
    public static Color highlightColor;
    public static Color selectColor;
    public static Color pieceColor;
    private Field possibleMove1;
    private Field possibleMove2;

    // Start is called before the first frame update
    public override void EnterPhase()
    {      
        _pieceSelector.SelectedPiece = null;

        foreach (var piece in FindObjectsOfType<PlayingPiece>().Where(p => p.State == PlayingPiece.CurrentState.Active))
            piece.SetSelectionEnabled(true);  
    }
    public override void LeavePhase()
    {
        foreach (var piece in FindObjectsOfType<PlayingPiece>())
            piece.SetSelectionEnabled(false);
        clearMoves();
        SelectedPiece.GetComponentInChildren<Renderer>().material.color = pieceColor;
        SelectedPiece = null;
    }
    public override void Setup()
    {
        defaultColor = new Color(1, 1, 1, 1);
        selectColor = new Color(1f, 0.55f, 0f, 0f);
        highlightColor = Color.yellow;
        TargetHiglighted = false;
        _pieceSelector = FindObjectOfType<SelectPieces>();
    }
    private void Update()
    {
        //if a new piece was selected update possible moves
        if(SelectedPiece != _pieceSelector.SelectedPiece)
        {
            clearMoves();
            if(SelectedPiece != null)
            {
                SelectedPiece.GetComponentInChildren<Renderer>().material.color = pieceColor;
            }
            else
            {
                pieceColor = _pieceSelector.SelectedPiece.GetComponentInChildren<Renderer>().material.color;
            }
            SelectedPiece = _pieceSelector.SelectedPiece;
            SelectedPiece.GetComponentInChildren<Renderer>().material.color = selectColor;

            highlightPossibleMoves();
        }
        
        //selection of field to move to
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.allCameras.First().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selected = hit.collider.gameObject.GetComponentInParent<Field>();
                if (selected != null)
                {
                    //is the selected field one of our calculated possible tagrets if yes highlight it differently and reset potential prior selections
                    if(selected == possibleMove1 || selected == possibleMove2)
                    {
                        if(SelectedField != null)
                        {
                            SelectedField.GetComponent<Renderer>().material.color = highlightColor;
                        }

                        SelectedField = selected;

                        SelectedField.GetComponent<Renderer>().material.color = selectColor;
                    }
                }
            }
        }
    }

    private void highlightPossibleMoves()
    {
        //clear highlighting before calculating new highlights
        clearMoves();
        possibleMove1 = SelectedPiece.CurrentPosition;
        possibleMove2 = SelectedPiece.CurrentPosition;
        for (int i = 0; i < DiceValue; i++)
        {
            int moves = possibleMove2.NextFields.Length;
            possibleMove1 = possibleMove1.NextFields.First();
            //check if more then one move is available to spot Target fields 
            if (moves == 2)
            {
                possibleMove2 = possibleMove2.NextFields[1];
            }
            else if(moves == 0)
            {
                possibleMove2 = possibleMove1;
            }
            else
            {
                possibleMove2 = possibleMove2.NextFields.First();
            }  
        }
        if (possibleMove1 == possibleMove2)
        {
            possibleMove2 = null;
            possibleMove1.GetComponent<Renderer>().material.color = highlightColor;
        }
        else
        {
            if (possibleMove2 is TargetField)
            {
                //check if the possible TargetField belongs to the owner
                TargetField tmp = (TargetField) possibleMove2;
                if (tmp.Owner == CurrentPlayer)
                {
                    //Check if the Possible targetfield is already occupied
                    bool moveAllowed = true;                
                    if(tmp.OnThis != null)
                        moveAllowed = false;
                    //if not occupied highlight as possible
                    if(moveAllowed)
                    {
                        TargetHiglighted = true;
                        possibleMove2.GetComponent<Renderer>().material.color = highlightColor;
                    }
                    else
                    {
                        possibleMove2 = null;
                    }  
                }            
                else
                {
                    possibleMove2 = null;
                }
            }
            possibleMove1.GetComponent<Renderer>().material.color = highlightColor;
        }
    }
    
    //Resets all highlighted possible moves and selected fields
    private void clearMoves()
    {
        if (possibleMove1 != null)
        {
            possibleMove1.GetComponent<Renderer>().material.color = defaultColor;
            possibleMove1 = null;
        }
        if(possibleMove2 != null)
        {
            possibleMove2.GetComponent<Renderer>().material.color = defaultColor;
            possibleMove2 = null;
        }
        if(SelectedField != null)
        {
            SelectedField.GetComponent<Renderer>().material.color = defaultColor;
            SelectedField = null;
        }
        TargetHiglighted = false;
    }

    public void OnGUI()
    {
        GUIStyle diceFont = new GUIStyle() { fontSize = 24 };
        diceFont.normal.textColor = CurrentPlayer.Color;

        GUI.Label(new Rect(Screen.width - 60, 10, 150, 150), DiceValue.ToString(), diceFont);

        if (SelectedPiece != null)
        {
            if(GUI.Button(new Rect(Screen.width - 150, 50, 150, 20), "Move Selected Piece"))
            {
                if(SelectedField != null)
                {
                    GetPhase<AnimateMove>().target = SelectedField;
                    GetPhase<AnimateMove>().selected = SelectedPiece;
                    GetPhase<AnimateMove>().stepCount = DiceValue;
                    ChangeToPhase<AnimateMove>();
                }
            }
            if (DiceValue == 6)
            {
                if (GUI.Button(new Rect(Screen.width - 150, 80, 150, 20), "View Selected Piece"))
                {
                    GetPhase<PeekPiece>().target = SelectedPiece;
                    ChangeToPhase<PeekPiece>();
                }
            }
        }
    }
}
