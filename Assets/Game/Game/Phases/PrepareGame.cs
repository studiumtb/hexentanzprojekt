using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareGame : GamePhase
{
    float currentMusicVolume = 100;
    float currentEnviromentVolume = 100;
    float currentEffectVolume = 100;

    GlobalSoundManager soundManager;

    public override void EnterPhase()
    {
    }
    public override void LeavePhase()
    {
    }
    public override void Setup()
    {        
        soundManager = FindObjectOfType<GlobalSoundManager>();
    }
    public void OnGUI()
    {
        GUI.Label(new Rect(5, Screen.height - 20, 150, 20), "Players:");
        int newPlayerNumber = (int)GUI.HorizontalSlider(new Rect(160, Screen.height - 20, Screen.width * 0.5f, 20), Parent.NumberOfPlayers, 2, 6);
        GUI.Label(new Rect(5, Screen.height - 40, 150, 20), "Pieces per Player:");
        int newPieceNumbber = (int)GUI.HorizontalSlider(new Rect(160, Screen.height - 40, Screen.width * 0.5f, 20), Parent.NumberOfFiguresPerPlayer, 2, 6);

        GUI.Label(new Rect(5, Screen.height - 80, 150, 20), "Music Volume:");
        float newMusicVolume = GUI.HorizontalSlider(new Rect(160, Screen.height - 80, Screen.width * 0.5f, 20), currentMusicVolume, 0, 100);
        GUI.Label(new Rect(5, Screen.height - 100, 150, 20), "Enviroment Volume:");
        float newEnviromentVolume = GUI.HorizontalSlider(new Rect(160, Screen.height - 100, Screen.width * 0.5f, 20), currentEnviromentVolume, 0, 100);
        GUI.Label(new Rect(5, Screen.height - 120, 150, 20), "Effect Volume:");
        float newEffectVolume = GUI.HorizontalSlider(new Rect(160, Screen.height - 120, Screen.width * 0.5f, 20), currentEffectVolume, 0, 100);

        if(newMusicVolume != currentMusicVolume)
        {
            currentMusicVolume = newMusicVolume;
            soundManager.Music = currentMusicVolume / 100;
        }
        if(newEnviromentVolume != currentEnviromentVolume)
        {
            currentEnviromentVolume = newEnviromentVolume;
            soundManager.Enviroment = currentEnviromentVolume / 100;
        }
        if(newEffectVolume != currentEffectVolume)
        {
            currentEffectVolume = newEffectVolume;
            soundManager.Effect = currentEffectVolume / 100;

            FindObjectOfType<DiceSounds>().PlayLanded();
        }

        if (newPlayerNumber != Parent.NumberOfPlayers || newPieceNumbber != Parent.NumberOfFiguresPerPlayer)
        {
            Parent.NumberOfPlayers = newPlayerNumber;
            Parent.NumberOfFiguresPerPlayer = newPieceNumbber;

            Parent.SetupGame();
        }

        if(GUI.Button(new Rect(Screen.width * 0.5f + 300, Screen.height - 20, 100, 20), "Start Game"))
        {
            Parent.StartNewGame();
            ChangeToPhase<StartTurn>();
        }
    }
}
