﻿using Assets.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PieceEnteredTarget : GamePhase
{
    enum AnimationPhase { Raise, Travel, Queue , Land }

    public PlayingPiece target;

    private float Speed = 1;
    private float travelHeight;
    private AnimationPhase currentPhase;
    private Game game;

    private Vector3 currentTargetPosition;
    public TargetField endField;
    bool finish;

    // Start is called before the first frame update
    public override void EnterPhase()
    {        
        currentPhase = AnimationPhase.Raise;        

        target.ShowColor = true;

        if (target.Owner == endField.Owner)
            finish = true;
        else
            finish = false;
    }
    public override void LeavePhase()
    {
        target = null;
    }
    public override void Setup()
    {
        Speed = FindObjectOfType<AnimateMove>().Speed;
        travelHeight = FindObjectsOfType<RingField>().Select(r => r.gameObject.transform.position.y).Max() + 5;
        currentPhase = AnimationPhase.Raise;
        game = FindObjectOfType<Game>();
        currentTargetPosition = Vector3.zero;
    }
    private void Update()
    {
        if (finish)
        {
            target.CurrentPosition = endField;
            ChangeToPhase<CheckForWinner>();
        }
        else if (currentPhase == AnimationPhase.Raise)
        {
            if (AnimateMove(target.gameObject, getTravelPosition(target.CurrentPosition.gameObject.transform.position), 0.01f, Speed))
            {
                currentPhase = AnimationPhase.Travel;
            }
        }
        else if (currentPhase == AnimationPhase.Travel)
        {
            endField = target.Owner.TargetFields.OrderBy(tf => tf.Hirarchy).Last(f => f.OnThis == null);
            if (AnimateMove(target.gameObject, getTravelPosition(endField.gameObject.transform.position), 0.01f, Speed))
            {
                currentTargetPosition = endField.gameObject.transform.position;
                currentPhase = AnimationPhase.Queue;
            }
        }
        else if (currentPhase == AnimationPhase.Queue)
        {
            if(AnimateMove(target.gameObject, getTravelPosition(currentTargetPosition), 0.01f, Speed))
            {
                currentPhase = AnimationPhase.Land;
            }
        }
        else if(currentPhase == AnimationPhase.Land)
        {
            if(AnimateMove(target.gameObject, currentTargetPosition, 0.01f, Speed))
            {
                finish = true;
            }
        }
    }

    private Vector3 getTravelPosition(Vector3 basePosition)
    {
        Vector3 newPosition = basePosition;
        newPosition.y = travelHeight;
        return newPosition;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(currentTargetPosition, 2);
    }
}