﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BeatOtherPiece : GamePhase
{
    enum AnimationPhase { Move, Attack, Raise, Show, TravelBack, Land }

    public PlayingPiece attacker;
    public PlayingPiece victim;
    private AnimationPhase currentPhase;

    private float Speed = 1;
    private float travelHeight = 0;
    private float peekDuration = 0;

    private float remainingDistance = 0;
    private float currentTravelDistance = 0;
    private float peekTimer = 0;

    private float waitForFlash = 0;

    [SerializeField]
    private float TravelSpeed;

    private Field startField;

    private Field currentAnimationPosition;

    private BeatenEffect effect;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        //Start with Moving the O
        startField = victim.CurrentPosition;
        currentAnimationPosition = startField;
        currentAnimationPosition = victim.CurrentPosition;
        currentPhase = AnimationPhase.Move;

        effect = victim.gameObject.GetComponentInChildren<BeatenEffect>();

        peekTimer = peekDuration;

        attacker.PlayMovingSound();
    }
    public override void LeavePhase()
    {
        attacker = null;
        victim = null;
    }
    public override void Setup()
    {
        Speed = FindObjectOfType<AnimateMove>().Speed;
        currentTravelDistance = FindObjectOfType<Game>().WalkBackDistance;
        peekDuration = FindObjectOfType<PeekPiece>().PeekDuration;

        travelHeight = FindObjectsOfType<RingField>().Select(r => r.gameObject.transform.position.y).Max() + 5;
        currentPhase = AnimationPhase.Move;        
    }
    private void Update()
    {
        if(currentPhase == AnimationPhase.Move)
        {
            if (AnimateMove(attacker.gameObject, victim.CurrentPosition.transform.position, 0.01f, Speed))
            {
                attacker.CurrentPosition = startField;
                currentPhase = AnimationPhase.Attack;

                victim.PlayMagic();
                effect.PlayEffect();
                effect.transform.parent = null;
                waitForFlash = 1.8f;
            }
        }
        else if(currentPhase == AnimationPhase.Attack)
        {
            waitForFlash -= Time.deltaTime;
            if(waitForFlash <= 0)
                currentPhase = AnimationPhase.Raise;
        }
        else if(currentPhase == AnimationPhase.Raise)
        {
            if(AnimateMove(victim.gameObject, getTravelPosition(startField.gameObject.transform.position), 0.01f, TravelSpeed))
            {
                currentPhase = AnimationPhase.Show;
                victim.ShowColor = true;
            }
        }
        else if(currentPhase == AnimationPhase.Show)
        {
            peekTimer -= Time.deltaTime;
            if (peekTimer <= 0)
            {
                currentPhase = AnimationPhase.TravelBack;
                remainingDistance = currentTravelDistance;
                victim.ShowColor = false;
            }
        }
        else if(currentPhase == AnimationPhase.TravelBack)
        {
            victim.CurrentPosition = null;
            var targetField = currentAnimationPosition.PreviousField;
            if (AnimateMove(victim.gameObject, getTravelPosition(targetField.gameObject.transform.position),0.01f, TravelSpeed))
            {
                remainingDistance--;

                if (remainingDistance <= 0)
                {
                    currentPhase = AnimationPhase.Land;
                }
                else
                {
                    currentAnimationPosition = targetField;
                }
            }
        }
        else if(currentPhase == AnimationPhase.Land)
        {
            if (currentAnimationPosition == startField)
            {
                currentTravelDistance--;
                remainingDistance = currentTravelDistance;
                currentPhase = AnimationPhase.TravelBack;
            }
            else if(currentAnimationPosition.OnThis != null)
            {                
                remainingDistance = currentTravelDistance;
                currentPhase = AnimationPhase.TravelBack;
            }
            else
            {
                if(AnimateMove(victim.gameObject, currentAnimationPosition.transform.position, 0.01f, TravelSpeed))
                {
                    effect.transform.parent = victim.transform;
                    effect.transform.localPosition = new Vector3(0, 0, 0);

                    victim.CurrentPosition = currentAnimationPosition;
                    currentPhase = AnimationPhase.Move;
                    ChangeToPhase<EndTurn>();
                }
            }
        }
    }

    private Vector3 getTravelPosition(Vector3 basePosition)
    {
        Vector3 newPosition = basePosition;
        newPosition.y = travelHeight;
        return newPosition;
    }

    private void OnGUI()
    {
        GUIStyle diceValue = new GUIStyle() { fontSize = 60 };
        diceValue.normal.textColor = CurrentPlayer.Color;
        GUI.Label(new Rect(Screen.width - 60, 10, 150, 150), ((currentPhase == AnimationPhase.Attack)?"1":"0"), diceValue);
    }
}