﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PeekPiece : GamePhase
{
    enum AnimationPhase { Raise, Rotate, Wait, RotateBack, Land }

    AnimationPhase currentPhase;

    public PlayingPiece target;
    
    private float PeekHeight;

    private Vector3 startPosition;
    private Quaternion startRotation;

    private float peekTimer = 0;

    private float startHeight;

    [SerializeField]
    private float RaiseSpeed;
    [SerializeField]
    public float PeekDuration;

    // Start is called before the first frame update
    public override void EnterPhase()
    {
        startPosition = target.gameObject.transform.position;
        startHeight = target.gameObject.transform.position.y;
        peekTimer = PeekDuration;
        currentPhase = AnimationPhase.Raise;
    }
    public override void LeavePhase()
    {
        target.ShowColor = false;
    }
    public override void Setup()
    {
        currentPhase = AnimationPhase.Raise;
        PeekHeight = FindObjectsOfType<RingField>().Select(r => r.gameObject.transform.position.y).Max() + 5;
        peekTimer = PeekDuration;
    }
    private void Update()
    {
       if(currentPhase == AnimationPhase.Raise)
        {
            if (target.gameObject.transform.position.y < PeekHeight)
            {
                target.gameObject.transform.position += new Vector3(0, RaiseSpeed * Time.deltaTime, 0);
            }
            else
            {
                target.ShowColor = true;
                currentPhase = AnimationPhase.Wait;
            }
        }
        else if(currentPhase == AnimationPhase.Wait)
        {
            peekTimer -= Time.deltaTime;

            if(peekTimer <= 0)
            {
                target.ShowColor = false;
                currentPhase = AnimationPhase.Land;
            }
        }
        else if(currentPhase == AnimationPhase.Land)
        {
            if (target.gameObject.transform.position.y > startHeight)
            {
                target.gameObject.transform.position -= new Vector3(0, RaiseSpeed * Time.deltaTime, 0);
            }
            else
            {
                target.gameObject.transform.position = startPosition;
                ChangeToPhase<EndTurn>();
            }
        }
    }
    public void OnGUI()
    {
        
    }
}