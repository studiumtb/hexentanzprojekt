﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Manager
{
    public static bool enableFullscreen = true;
    public static bool enablePostProcessing = true;
    public static int qualityLevel = 2;
}
