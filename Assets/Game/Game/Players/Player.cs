using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int Id;
    public string Name;
    public Color Color;

    public StartField[] StartFields;
    public TargetField[] TargetFields;

    public PlayingPiece[] PlayingPieces;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        foreach (var piece in PlayingPieces)
        {
            if (piece != null)
                Destroy(piece.gameObject);
        }
    }
}
