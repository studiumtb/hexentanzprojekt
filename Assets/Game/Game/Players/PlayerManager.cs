using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    Color[] _defaultColors = new Color[]
    {
        Color.red,
        Color.green,
        Color.blue,
        Color.magenta,
        Color.yellow,
        Color.cyan
    };

    public Transform playerTemplate;
    public List<Player> players = new List<Player>();

    public Transform playingPiece;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Generate Player Objects
    /// </summary>
    public void SetupPlayers()
    {
        //Delete all currently existing Players
        foreach(Player player in players)
        {
            Destroy(player.gameObject);
        }
        players.Clear();
        //Create Players
        var game = FindObjectOfType<Game>();
        for(int i = 0; i < game.NumberOfPlayers; i++)
        {
            var player = Instantiate(playerTemplate);
            var data = player.GetComponent<Player>();
            data.Id = i;
            data.Name = "Player" + i;
            data.Color = _defaultColors[i];
            players.Add(data);
            player.parent = gameObject.transform;
        }
    }

    public void GeneratePlayingPieces()
    {
        foreach(Player player in players)
        {
            List<PlayingPiece> allPieces = new List<PlayingPiece>();
            foreach(StartField start in player.StartFields)
            {
                var newPiece = Instantiate(playingPiece);
                newPiece.transform.position = start.transform.position;
                PlayingPiece piece = newPiece.GetComponent<PlayingPiece>();
                piece.CurrentPosition = start;
                allPieces.Add(piece);
                piece.Owner = player;
                piece.transform.parent = player.gameObject.transform;
            }
            player.PlayingPieces = allPieces.ToArray();
        }
    }
}
