﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Game
{
    internal static class GenerationHelper
    {

        /// <summary>
        /// Calculate Position from start, angle, distance and optional jitter
        /// </summary>
        /// <param name="start"></param>
        /// <param name="angle"></param>
        /// <param name="distance"></param>
        /// <param name="jitter"></param>
        /// <returns></returns>
        public static Vector3 calculatePosition(Vector3 start, float angle, float distance, float jitter = 0)
        {
            var q = Quaternion.AngleAxis(angle, Vector3.up);
            var newPosition = start + q * Vector3.left *
                (distance + UnityEngine.Random.Range(-jitter, jitter));

            return newPosition;
        }

        public static float calculateAngle(Vector3 start, Vector3 target)
        {
            return Vector2.Angle(target - start, Vector2.right);
        }

        /// <summary>
        /// Get Max Height of 
        /// Area
        /// </summary>
        /// <param name="position">Position in Space</param>
        /// <param name="radius">Size of the Area to Sample</param>
        /// <param name="terrain">Terrain to Sample</param>
        /// <returns></returns>
        public static float samplePosition(Vector3 position, float radius, Terrain terrain)
        {
            Vector3[] samplePositions = new Vector3[]
            {
                position,
                position + new Vector3(radius,0,0),
                position + new Vector3(0,0,radius),
                position + new Vector3(0,0,-radius),
                position + new Vector3(-radius,0,0)
            };

            var heights = samplePositions.Select(p => SampleHeightAtPosition(p, terrain));
            float max = heights.Max();
            return max;
        }

        public static float samplePosition(Vector3 position, GameObject toPlace, Terrain terrain)
        {
            Renderer r = toPlace.gameObject.GetComponent<Renderer>();
            if (r != null)
            {
                return samplePosition(position, r.bounds.max.y, terrain);
            }
            else
            {
                return SampleHeightAtPosition(position, terrain);
            }
        }

        private static float SampleHeightAtPosition(Vector3 position, Terrain terrain)
        {
            return terrain.SampleHeight(position);
        }
    }
}
