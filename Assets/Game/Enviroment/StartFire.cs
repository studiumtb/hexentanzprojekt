using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFire : MonoBehaviour
{
    public Player owner;

    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem.MainModule mm = gameObject.GetComponentInChildren<ParticleSystem>().main;
        mm.startColor = owner.Color;

        foreach(var ps in gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            var psmm = ps.main;
            psmm.startColor = owner.Color;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
