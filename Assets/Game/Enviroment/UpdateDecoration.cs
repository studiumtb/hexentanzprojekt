using Assets.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateDecoration : MonoBehaviour
{
    [SerializeField]
    private Transform FirePlace;
    [SerializeField]
    private float HouseOffset;
    [SerializeField]
    private Transform House;

    List<GameObject> _generatedDecoration = new List<GameObject>();

    private GeneratePath _leveGenerator;
    private PlayerManager _playerManager;
    private GeneratePath LevelGenerator
    {
        get
        {
            if(_leveGenerator == null)
                _leveGenerator = FindObjectOfType<GeneratePath>();

            return _leveGenerator;
        }
    }
    private Terrain _terrain;
    private Terrain Terrain
    {
        get
        {
            if(_terrain == null)
                _terrain = FindObjectOfType<Terrain>();

            return _terrain;
        }
    }
    private Game _game;
    private Game Game
    {
        get
        {
            if(_game == null)
                _game = FindObjectOfType<Game>();

            return _game;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _playerManager = FindObjectOfType<PlayerManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupDecorations()
    {
        CleanUp();
        CleanUpPaths();
        DecorateStarts();
    }

    private void CleanUp()
    {
        foreach (GameObject go in _generatedDecoration)
            Destroy(go);

        _generatedDecoration.Clear();
    }

    private void CleanUpPaths()
    {

    }

    private void DecorateStarts()
    {
        int index = 0;
        foreach(Vector3 center in LevelGenerator.getStarts())
        {
            DecorateStart(center, index);
            index++;
        }
    }
    private void DecorateStart(Vector3 center, int playerIndex)
    {
        var fireInCenter = Instantiate(FirePlace);
        Vector3 firePosition = center;
        firePosition.y = GenerationHelper.samplePosition(center, FirePlace.gameObject, Terrain);
        fireInCenter.position = firePosition;
        fireInCenter.transform.parent = gameObject.transform;
        fireInCenter.GetComponent<StartFire>().owner = _playerManager.players[playerIndex];

        _generatedDecoration.Add(fireInCenter.gameObject);

        /*
        for (int i = 0; i < Game.NumberOfFiguresPerPlayer; i++)
        {
            float radius = LevelGenerator.startRadius * 1.5f;
            float angle = 0;

            Vector3 housePosition = GenerationHelper.calculatePosition(center, angle, radius, 0.4f);

            housePosition.y = GenerationHelper.samplePosition(housePosition, House.gameObject, Terrain);
        }
        */
    }
}
