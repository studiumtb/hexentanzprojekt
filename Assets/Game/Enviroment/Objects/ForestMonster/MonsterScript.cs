using Assets.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterScript : MonoBehaviour
{
    [SerializeField]
    float eyeTimer = 0;

    float currentAngle = 0;

    [SerializeField]
    float visibleMax = 0;
    [SerializeField]
    float visibleMin = 0;

    [SerializeField]
    float invisibleMax = 0;
    [SerializeField]
    float invisibleMin = 0;

    [SerializeField]
    float maxDistance = 0;
    [SerializeField]
    float minDistance = 0;

    [SerializeField]
    float minimumOffset = 0;

    [SerializeField]
    bool wasSeen = false;

    public Terrain terrain;

    bool IsSeen
    {
        get
        {
            return gameObject.GetComponentInChildren<Renderer>().isVisible;
        }
    }

    bool _visible = true;
    bool Visible
    {
        get
        {
            return _visible;  
        }
        set
        {
            _visible = value;
            foreach(var render in gameObject.GetComponentsInChildren<Renderer>())
            {
                render.enabled = _visible;
            }
            foreach(var light in gameObject.GetComponentsInChildren<Light>())
            {
                light.enabled = _visible;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Visible = false;
        wasSeen = false;
        gameObject.transform.LookAt(Vector3.zero);
        eyeTimer = Random.Range(invisibleMin, invisibleMax);
        Reposition();
    }

    // Update is called once per frame
    void Update()
    {
        if(Visible)
        {
            if (!wasSeen)
                wasSeen = IsSeen;

            if(wasSeen)
            {
                eyeTimer -= Time.deltaTime;

                if(eyeTimer <= 0)
                {
                    Visible = false;
                    eyeTimer = Random.Range(invisibleMin, invisibleMax);
                    Reposition();
                    wasSeen = false;
                }
            }
        }
        else
        {
            eyeTimer -= Time.deltaTime;

            if(eyeTimer <= 0)
            {
                eyeTimer = Random.Range(visibleMin, visibleMax);
                Visible = true;
            }
        }
    }

    void Reposition()
    {
        float newOffset = Random.Range(minimumOffset, 360 - minimumOffset);
        currentAngle = (currentAngle + newOffset) % 360;
        float newDistance = Random.Range(minDistance, maxDistance);
        var newPosition = GenerationHelper.calculatePosition(Vector3.zero, currentAngle, newDistance);

        gameObject.transform.position = gameObject.transform.position;

        gameObject.transform.LookAt(Vector3.zero);
    }
}
