using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlobalSoundManager : MonoBehaviour
{
    [Range(0f, 1f)]
    public float Music;
    [Range(0f, 1f)]
    public float Enviroment;
    [Range(0f, 1f)]
    public float Effect;

    float _music;
    float _enviroment;
    float _effect;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(_music != Music)
        {
            _music = Music;
            foreach (SoundSource source in FindObjectsOfType<SoundSource>().Where(s => s.type == SoundSource.SoundType.Music))
                source.SetVolume(_music);
        }
        if(_enviroment != Enviroment)
        {
            _enviroment = Enviroment;
            foreach (SoundSource source in FindObjectsOfType<SoundSource>().Where(s => s.type == SoundSource.SoundType.Enviroment))
                source.SetVolume(_enviroment);
        }
        if(_effect != Effect)
        {
            _effect = Effect;
            foreach (SoundSource source in FindObjectsOfType<SoundSource>().Where(s => s.type == SoundSource.SoundType.Effect))
                source.SetVolume(_effect);
        }
    }
}
