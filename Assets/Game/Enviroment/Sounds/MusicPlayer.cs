using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MusicPlayer : SoundSource
{
    public AudioClip[] _musicPieces;
    private int currentPiece = 0;

    private AudioSource _player1;
    private AudioSource _player2;

    private int _currentSource;

    private float _volumeLevel;
    private float _baseLevel;

    private float _fadeProgress;

    [SerializeField]
    private float fadeSpeed;

    AudioSource CurrentSource
    {
        get
        {
            if(_currentSource == 0)
                return _player1;
            else
                return _player2;
        }
    }
    AudioSource NextSource
    {
        get
        {
            if (_currentSource == 0)
                return _player2;
            else
                return _player1;
        }
    }

    // Start is called before the first frame update
    void Start()
    {        
        _player1 = GetComponents<AudioSource>()[0];
        _player2 = GetComponents<AudioSource>()[1];

        ShuffleMusic();

        _baseLevel = _player1.volume;
        _player1.clip = _musicPieces.First();
        _player1.Play();
        this.type = SoundType.Music;
        _fadeProgress = 1;
        PrepareNextPiece();
    }

    // Update is called once per frame
    void Update()
    {
        if (_fadeProgress < 1)
        {
            CrossFade();
            _fadeProgress += Time.deltaTime * fadeSpeed;
            if (_fadeProgress == 0)
            {
                CurrentSource.Stop();
                _currentSource = (_currentSource == 0) ? 1 : 0;
            }
        }
        else
        {
            CurrentSource.volume = _volumeLevel;

            if (CurrentSource.clip.length - CurrentSource.time < Time.deltaTime * fadeSpeed * 1000 || CurrentSource.clip == null)
            {
                _fadeProgress = 0;
                PrepareNextPiece();
                NextSource.Play();
            }
        }
    }

    void PrepareNextPiece()
    {        
        currentPiece = (currentPiece + 1) % _musicPieces.Count();
        NextSource.Stop();
        NextSource.clip = _musicPieces[currentPiece];
    }

    void CrossFade()
    {
        CurrentSource.volume = volumeAtFadePercent(1 - _fadeProgress * 2);
        NextSource.volume = volumeAtFadePercent((_fadeProgress - 0.5f)*2);
    }

    float volumeAtFadePercent(float fadeProgress)
    {
        return _volumeLevel * fadeProgress;
    }

    public override void SetVolume(float value)
    {
        _volumeLevel = _baseLevel * value;
    }

    private void ShuffleMusic()
    {
        _musicPieces = _musicPieces.OrderBy(p => Random.value).ToArray();
    }
}
