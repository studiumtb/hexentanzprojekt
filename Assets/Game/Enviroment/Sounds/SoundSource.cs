using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSource : MonoBehaviour
{
    public enum SoundType { Music, Enviroment, Effect }

    public SoundType type;

    private float baseVolume;

    // Start is called before the first frame update
    void Start()
    {        
        baseVolume = gameObject.GetComponents<AudioSource>()[0].volume;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void SetVolume(float value)
    {
        foreach (var sound in GetComponents<AudioSource>())
            sound.volume = baseVolume * value;
    }
}
